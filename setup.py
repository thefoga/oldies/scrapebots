# !/usr/bin/python3
# coding: utf_8

""" Setups library and install dependencies """

from setuptools import setup

LITTLE_DESCRIPTION = "bots to scrape web-platforms"
DESCRIPTION = \
    "scrapebots\n" + LITTLE_DESCRIPTION + "\n\
    Install\n\
    - $ pip3 install . --upgrade --force-reinstall\n\
    \n\
    Questions and issues\n\
    The Github issue tracker is only for bug reports and feature requests."
VERSION = open("VERSION").readlines()[0]

setup(
    name="scrapebots",
    version=VERSION,
    author="sirfoga",
    author_email="sirfoga@protonmail.com",
    description="bots scrape web",
    long_description=DESCRIPTION,
    keywords="crawler scraper bot",
    url="https://github.com/sirfoga/scrapebots",
    install_requires=[
        'pyhal',
        'pymongo',
        'aiohttp',
        'aiosocks',
        'beautifulsoup4',
        'colorama',
        'lxml'
    ],
    test_suite="tests"
)
