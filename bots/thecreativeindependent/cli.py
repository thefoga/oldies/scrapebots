# -*- coding: utf-8 -*-

""" Downloads highlighted text from article """

import argparse

from thecreativeindependent.models import TheCreativeIndependentBot


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage="-a <article web address>\n"
                                           "-h for full usage")

    parser.add_argument("-a", dest="article",
                        help="article web address",
                        type=str,
                        required=True)

    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    article = args.article

    return article


def main():
    article = parse_args(create_args())
    bot = TheCreativeIndependentBot(article)
    bot.print_highlighted_text()


if __name__ == '__main__':
    main()
