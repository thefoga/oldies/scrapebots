# -*- coding: utf-8 -*-

""" Downloads highlighted text from article """

from colorama import Back, Fore, Style

from hal.internet.web import Webpage

DEFAULT_COLORS = {
    Back.BLACK: '#000000',
    Back.RED: '#FF0000',
    Back.GREEN: '#00FF00',
    Back.YELLOW: '#FFFF00',
    Back.BLUE: '#0000FF',
    Back.MAGENTA: '#FF00FF',
    Back.CYAN: '#00FFFF',
    Back.WHITE: '#FFFFFF'
}


def get_nearest_color(color_code, color_base=16):
    if color_code.startswith('#'):
        color_code = color_code[1:]  # remove starting #

    num_repr = int(color_code, color_base)
    color_repr = {
        k: int(v[1:], color_base)  # remove starting #
        for k, v in DEFAULT_COLORS.items()
    }
    color_diffs = {
        k: abs(v - num_repr)  # diff
        for k, v in color_repr.items()
    }

    nearest_color = min(color_diffs, key=color_diffs.get)  # based on value

    return nearest_color


def get_foreground_color(background_color):
    nearest_color = get_nearest_color(background_color)
    is_light_color = (nearest_color == Back.YELLOW or
                      nearest_color == Back.WHITE)
    if is_light_color:
        return Fore.BLACK

    return Fore.WHITE


class TheCreativeIndependentBot:
    HIGHLIGHT_COLOR_MARKER = 'background-color: #'
    HIGHLIGHT_COLOR_LENGTH = 6
    DEFAULT_HIGHLIGHT_COLOR = '#000000'

    def __init__(self, web_address):
        self.web_page = Webpage(web_address)
        self.web_page.get_html_source()  # load web page

    def get_text(self):
        return self.web_page.soup.find('div', {'class': 'reading-body'})

    def get_highlighted_text(self):
        text = self.get_text()
        highlighted_texts = text.find_all('span', {'class': 'highlight'})
        highlighted_texts = [
            t.text.strip()
            for t in highlighted_texts
        ]
        return '\n'.join(highlighted_texts)

    def get_highlight_color(self):
        try:
            color_index = self.web_page.source.index(
                self.HIGHLIGHT_COLOR_MARKER
            ) + len(self.HIGHLIGHT_COLOR_MARKER) - 1  # include #
            color_end_index = color_index + self.HIGHLIGHT_COLOR_LENGTH
            color = self.web_page.source[color_index: color_end_index]
            return get_nearest_color(color)
        except:
            return get_nearest_color(self.DEFAULT_HIGHLIGHT_COLOR)

    def print_highlighted_text(self):
        text = self.get_highlighted_text()
        highlight_color = self.get_highlight_color()
        foreground_color = get_foreground_color(DEFAULT_COLORS[highlight_color])
        print(highlight_color, foreground_color, text, Style.RESET_ALL)
