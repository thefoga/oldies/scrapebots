# -*- coding: utf-8 -*-

""" Update mongodb database """

import asyncio
import os
import time
import traceback
from datetime import datetime
from datetime import timedelta

import aiohttp
from aiosocks.connector import ProxyConnector, ProxyClientRequest
from hal.profile.mem import get_memory_usage, force_garbage_collect
from hal.time.profile import print_time_eta, get_time_eta
from pymongo import MongoClient

from .parsers import get_url_of_page, get_list_of_stages, \
    get_standings_of_stage, get_stage_details_from_url

LOG_FILE = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                        str(os.path.basename(__file__)).split(".")[
                            0] + "-" + str(int(time.time())) + ".log")
MIN_YEAR_PAGE = 1903  # minimum year of tour
MAX_YEAR_PAGE = 2016  # maximum year of tour

DATABASE_NAME = "letour-stages"  # name of mongodb database to use
MONGODB_CLIENT = MongoClient()  # mongodb client
# mongodb_client.drop_database(DATABASE_NAME)  # remove all previous data
DATABASE = MONGODB_CLIENT[
    DATABASE_NAME
]  # database to use
for coll in DATABASE.collection_names():
    DATABASE[coll].create_index("num", unique=True)  # set primary key


async def try_and_fetch(url, max_attempts=8, time_delay_between_attempts=1):
    """
    :param url: str
        Url to fetch
    :param max_attempts: int
        Max number of attempts to get page
    :param time_delay_between_attempts: float
        Number of seconds to wait between 2 consecutive attempts
    :return: str
        Body of page with url or null
    """

    for _ in range(max_attempts):
        try:
            conn = ProxyConnector(remote_resolve=True)
            async with aiohttp.ClientSession(
                    connector=conn,
                    request_class=ProxyClientRequest
            ) as session:
                async with session.get(
                        url,
                        proxy="socks5://127.0.0.1:9150"
                ) as response:  # use tor
                    body = await response.text(encoding='latin-1')
                    RAW_SOURCES.append({
                        "url": str(url),
                        "html": str(body)
                    })  # add url and page source

                    print_time_eta(
                        get_time_eta(
                            len(RAW_SOURCES),
                            TOTAL,
                            START_TIME
                        ),  # get ETA
                        note="Got HTML"
                    )  # debug info
                    return body
        except Exception as e:
            time.sleep(time_delay_between_attempts)
            traceback.print_exc()
            print("Cannot get url " + str(url))
            print(str(e))
    return None


async def bound_fetch(sem, url):
    """
    :param sem: Semaphore
        Asynchronous driver
    :param url: str
        Url of page to fetch
    :return: void
        Asynchronously fetches url
    """

    async with sem:
        await try_and_fetch(url, max_attempts=1, time_delay_between_attempts=0)


async def fetch_urls(list_of_urls, max_concurrent=200):
    """
    :param list_of_urls: [] of str
        List of urls to fetch
    :param max_concurrent: int
        Max number of concurrent connections
    :return: void
        Fetches url
    """

    tasks = []
    sem = asyncio.Semaphore(max_concurrent)
    for url in list_of_urls:
        task = asyncio.ensure_future(bound_fetch(sem, url))
        tasks.append(task)

    responses = asyncio.gather(*tasks)
    await responses


if __name__ == "__main__":
    START_TIME_OVERALL = time.time()

    print("\t0 - Getting URLs list")
    URLS_LIST = [get_url_of_page(year) for year in
                 range(MIN_YEAR_PAGE, MAX_YEAR_PAGE + 1)]  # get list of urls
    TOTAL = len(URLS_LIST)
    RAW_SOURCES = []  # list of raw HTML pages to parse

    print("\t1 - Downloading years pages")
    START_TIME = time.time()
    LOOP = asyncio.get_event_loop()
    FUTURE_LOOPS = asyncio.ensure_future(fetch_urls(URLS_LIST))  # fetch
    LOOP.run_until_complete(FUTURE_LOOPS)

    print("\t2 - Getting list of all stages")
    START_TIME = time.time()
    URLS_LIST = []
    TOTAL = len(RAW_SOURCES)
    for i, value in enumerate(RAW_SOURCES):
        URLS_LIST += get_list_of_stages(
            value["html"]
        )  # get list of stages in page
        print_time_eta(
            get_time_eta(
                i + 1,
                TOTAL,
                START_TIME
            ),  # get ETA
            note="Got stage list"
        )  # debug info

    print("\t3 - Downloading stages pages")
    RAW_SOURCES = []  # list of raw HTML pages to parse
    TOTAL = len(URLS_LIST)

    START_TIME = time.time()
    LOOP = asyncio.get_event_loop()
    FUTURE_LOOPS = asyncio.ensure_future(fetch_urls(URLS_LIST))  # fetch
    LOOP.run_until_complete(FUTURE_LOOPS)
    LOOP.close()

    print("\t4 - Garbage-collecting useless stuff")
    URLS_LIST = None
    force_garbage_collect()

    print("\t5- Parsing stages pages")
    TOTAL = len(RAW_SOURCES)
    START_TIME = time.time()
    for i, value in enumerate(RAW_SOURCES):
        stage_standings = get_standings_of_stage(
            value["html"]
        )
        stage_details = get_stage_details_from_url(value["url"])
        d = {
            "num": stage_details["id"],
            "standings": stage_standings
        }

        try:
            DATABASE[str(stage_details["year"])].insert_one(d)
            RAW_SOURCES[i] = None  # release memory
        except Exception as exception:
            print(str(exception))

        print_time_eta(
            get_time_eta(
                i + 1,
                TOTAL,
                START_TIME
            ),  # get ETA
            note="Saved to database"
        )  # debug info

    MONGODB_CLIENT.close()  # close mongodb connection

    END_TIME_OVERALL = time.time()
    DELTA_TIME_OVERALL = END_TIME_OVERALL - START_TIME_OVERALL
    DELTA_MEM_OVERALL = get_memory_usage()

    print(
        "Done downloading and saving data to mongodb database \"" + str(
            DATABASE_NAME) + "\". Job started at",
        datetime.fromtimestamp(START_TIME_OVERALL).strftime(
            "%Y-%m-%d %H:%M:%S"), "completed at",
        datetime.fromtimestamp(END_TIME_OVERALL).strftime("%Y-%m-%d %H:%M:%S"),
        ", took",
        str(timedelta(seconds=int(DELTA_TIME_OVERALL))), "and ~",
        str(DELTA_MEM_OVERALL), "MB to complete."
    )  # debug info
