# -*- coding: utf-8 -*-

""" Parse, fix and save data """

import pandas as pd


def get_list_queries(df):
    """
    :param df:pandas.DataFrame
        Content of .csv file
    :return: [] of {}
        List of queries with attributes in input data
    """

    return list(df.T.to_dict().values())  # to list


def get_data_from_csv(path_in):
    """
    :param path_in: str
        File to use as input
    :return: pandas.DataFrame
        Content of .csv file
    """

    return pd.read_csv(path_in)


def save_dicts_to_csv(dicts, path_out):
    """
    :param dicts: List of dicts
        Content to save
    :param path_out: str
        File to use as output
    :return: void
        Saves data to .csv file
    """

    df = pd.DataFrame(dicts)
    df.to_csv(
        path_out,
        sep=",",
        quotechar="\"",
        index=False
    )  # save to output file
