# -*- coding: utf-8 -*-

import pandas as pd

VALUE_NOT_FOUND = str(
    "DNF")  # value to put when data cannot be found (or some errors occur)


def append_to_file(f, s):
    """
    :param f: str
        Path to file to append stuff to
    :param s: str
        Stuff to append
    :return: void
        Appends stuff to file
    """

    try:
        with open(f, "a") as o:
            o.write(str(s))
            o.write("\n")
    except Exception as e:
        print("Cannot append", str(s), "to", str(f))
        print(str(e))


def get_dicts_from_csv(path_file):
    """
    :param path_file: str
        File to parse
    :return: [] of {}
        List of data details in file
    """

    d = pd.read_csv(path_file).T.to_dict()
    return list(d.values())
