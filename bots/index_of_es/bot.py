# -*- coding: utf-8 -*-

""" Scrapes index-of.es web-page """

import os
import urllib.parse
import urllib.request

import pandas as pd
from bs4 import BeautifulSoup
from hal.internet.web import Webpage, download_to_file

SCRIPT_FOLDER = os.path.dirname(os.path.realpath(__file__))
CURRENT_FOLDER = os.getcwd()
BASE_URL = "http://index-of.es/"
CHAR_FIXES = {
    "%20": " ",
    "%80": "€",
    "%84": """,
    "%92": """,
    "%93": """,
    "%94": """,
    "%96": "-",
    "%99": ""
}


def is_file_url(url):
    """
    :param url: str
        Url pointing to a file or a folder
    :return: bool
        True iff url points to a file
    """

    return not url.endswith("/")


def is_http_status_good(status):
    """
    :param status:  int
        Status of response http server
    :return: bool
        True iff status is considered good and can keep going with process
    """

    return status < 400


def get_links_in_page(page, url):
    """
    :param page: str
        Raw HTML page
    :param url: str
        Url of page
    :return: (generator of) [] of str
        List of urls to download found in page
    """

    soup = BeautifulSoup(page, "lxml")  # parser
    table = soup.find("pre")
    for link in table.find_all("a")[4:]:
        if len(link.text) > 2:
            link = link.text.strip()

            if "Parent Directory" not in link:
                link = urllib.parse.urljoin(
                    url, link.replace(" ", "%20")
                )  # complete relative url
                link = link.replace("\\x", "%")
                link = link.replace("\\", "")
                yield link


def get_all_urls(start_url=BASE_URL, verbose=True):
    """
    :param start_url: str
        Url where to start scraping
    :param verbose: bool
        True iff you want verbose mode activated: prints messages on urls
        discovered
    :return: [] of str
        List of url found by recursively scrape web-pages
    """

    web_page = Webpage(start_url)
    web_page.get_html_source()

    links_in_page = list(
        get_links_in_page(web_page.source, start_url)
    )
    folder_links = [
        link for link in links_in_page if not is_file_url(link)
        ]  # get urls to scrape next
    file_links = [
        link for link in links_in_page if is_file_url(link)
        ]  # get actual files ready to download

    if verbose:
        print(
            start_url, ": found", len(file_links), "files and",
            len(folder_links), "folders"
        )

    for folder_link in folder_links:
        links = get_all_urls(folder_link)  # find urls there
        file_links += links  # add to overall list

    return file_links


def get_download_path(url):
    """
    :param url: str
        Url to download
    :return: str
        Path from main server to given file
    """

    fixed_url = str(url).strip()
    for fix in CHAR_FIXES:
        fixed_url = fixed_url.replace(
            fix,
            CHAR_FIXES[fix]
        )
    paths = fixed_url.split("http://index-of.es/")[-1]
    paths = paths.replace("%20", " ")
    paths = paths.replace("%92", "'")
    paths = paths.split("/")
    return os.path.join(*paths)


def download_files_list(output_file="files.csv"):
    """
    :param output_file: str
        Name of output file
    :return: void
        Downloads list of downloadable files from web-page, and saves list
        to file
    """

    all_urls = get_all_urls()
    data_frame = pd.DataFrame(all_urls)
    data_frame.to_csv(
        path_or_buf=output_file,
        sep=",",
        quotechar="\"",
        index=False,
        header=False
    )


def download(url, root_folder):
    """
    :param url: str
        Url to download
    :param root_folder: str
        Path to folder where to save data (main folder)
    :return: void
        Saves files with given url in folder
    """

    local_file = os.path.join(
        root_folder,
        get_download_path(url)
    )
    output_folder = os.path.dirname(local_file)
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)  # create necessary directories

    download_to_file(url, local_file, cookies={"240plan": "R130201388"})
    print(url, local_file)


def download_files(input_file=os.path.join(SCRIPT_FOLDER, "files.csv"),
                   root_folder=CURRENT_FOLDER):
    """
    :param input_file: str
        Name of input file with urls to fetch
    :param root_folder: str
        Path to folder where can save downloaded data
    :return: void
        Download urls from file and saves data to folder
    """

    urls = open(input_file, "r").readlines()  # one url per line
    output_folder = os.path.join(root_folder, "index-of.es")
    for url in urls:
        download(url, output_folder)


if __name__ == '__main__':
    download_files()
