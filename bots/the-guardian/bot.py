# -*- coding: utf-8 -*-

""" Parses The Guardian web-page """

from bs4 import BeautifulSoup


def is_title(raw):
    return len(raw) > 2


def parse(html):
    soup = BeautifulSoup(html, 'html.parser')
    titles = [x.text for x in soup.find_all('h2')[:57] if is_title(x)]
    titles = [x[2:].replace('–', '-') for x in titles]
    print(titles)


def main():
    html = open('index.html', 'r').read()
    parse(html)


if __name__ == '__main__':
    main()
