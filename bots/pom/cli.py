# -*- coding: utf-8 -*-

""" Downloads .pdf about Italian national maths competitions """

import argparse
from datetime import datetime

from pom.models import MathsItalianCompetition

MIN_YEAR = 1994
MAX_YEAR = datetime.now().year


def create_args():
    """
    :return: ArgumentParser
        Parser that handles cmd arguments.
    """

    parser = argparse.ArgumentParser(usage="-o <output folder>\n"
                                           "-h for full usage")

    parser.add_argument("-o", dest="out",
                        help="output folder",
                        type=str,
                        required=True)
    parser.add_argument("-c", dest="competition",
                        help="e.g 'Archimede'; "
                             "if not specified -> all available",
                        type=str,
                        required=False)
    parser.add_argument("-y", dest="years",
                        help="e.g 1997 or 1996 - 1999; "
                             "if not specified -> all available",
                        type=str,
                        required=False)

    return parser


def parse_args(parser):
    """
    :param parser: ArgumentParser
        Object that holds cmd arguments.
    :return: tuple
        Values of arguments.
    """

    args = parser.parse_args()

    output_folder = args.out

    competition = MathsItalianCompetition.ALL
    if args.competition:
        competition = args.competition.strip()
        assert competition in MathsItalianCompetition.ALL

    years = list(range(MIN_YEAR, MAX_YEAR + 1, 1))  # all since beginning
    if args.years:
        if '-' in args.years:  # multiple years specified
            years = args.years.split('-')
            min_year = int(years[0].strip())
            max_year = int(years[-1].strip())
            years = range(min_year, max_year + 1, 1)
        else:
            years = [int(args.years.strip())]

        for year in years:
            assert MIN_YEAR <= year <= MAX_YEAR

    return output_folder, competition, years


def main():
    output_folder, competition, years = parse_args(create_args())


if __name__ == '__main__':
    main()
