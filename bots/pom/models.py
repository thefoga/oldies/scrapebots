# -*- coding: utf-8 -*-

""" Downloads .pdf about Italian national maths competitions """

from enum import Enum


class MathsItalianCompetition(Enum):
    ARCH = ['Archimede']
    PROV = ['Provinciali']
    CESE = ['Cesenatico']
    SQUAD = ['Gara Squadre']
    IMO = ['IMO']
    PRIME = ['PRIME']

    ALL = [ARCH[0], PROV[0], CESE[0], SQUAD[0], IMO[0], PRIME[0]]
