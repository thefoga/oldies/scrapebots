# -*- coding: utf-8 -*-

class NewReleasesSpider(object):
    """ Scrape RottenTomatoes website to fetch content about various models """

    def __init__(self):
        object.__init__(self)

    def get_new_this_week(self):
        pass

    def get_box_office(self):
        pass

    def get_coming_soon(self):
        pass
