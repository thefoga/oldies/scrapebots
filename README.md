<div align="center">
<h1>scrapebots</h1>
<em>bots to scrape web-platforms</em></br></br>
</div>

<div align="center">
<a href="https://landscape.io/github/sirfoga/scrapebots/master"><img alt="Code Health" src="https://landscape.io/github/sirfoga/scrapebots/master/landscape.svg?style=flat"></a> <a href="https://codeclimate.com/github/sirfoga/scrapebots"><img alt="Code Climate" src="https://lima.codeclimate.com/github/sirfoga/scrapebots/badges/gpa.svg"></a> <img alt="pylint Score" src="https://mperlet.de/pybadge/badges/7.41.svg">
</div>

<div align="center">
<a href="https://opensource.org/licenses/MIT"><img src="https://img.shields.io/badge/License-MIT-blue.svg"></a> <a href="https://opensource.org/licenses/MIT"><img alt="Open Source Love" src="https://badges.frapsoft.com/os/v1/open-source.svg?v=103"></a> <a href="https://github.com/sirfoga/scrapebots/issues"><img alt="Contributions welcome" src="https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat"></a>
</div>


## Available bots
| name | description | data size (# entries or dimension) |
| ------------- | ------------- | ------------- |
| [andreadd.it](bots/misc/andreadd.py) | download [Polimi](http://www.polimi.it/en/) books and notes | ~1GB |
| [Amazon](bots/amazon/amazon_items_scraper.py)  | parse items in classic Amazon page | DNF |
| [connemarathon](bots/conne_marathon/bot.py) | connemarathon (since 2002)  | ~30000 |
| [FSG media downloader](bots/misc/fsgmedia-downloader.py) | download pictures from (Samsung) FSG servers | ~4TB |
| [Github](bots/github/tester.py)  | use API token to get info about users, repos | DNF |
| [Google Images](bots/misc/google_image.py) | download images  | DNF |
| [Hackerrank](bots/misc/hackerrank.py) | download problems statements and solutions | DNF |
| [IMDB](bots/imdb) | scrape largets dataset of movies | DNF |
| [index-of.es](bots/index_of_es/bot.py) | programming books | ~25000 |
| [ITA industries](bots/ita_industries/cli.py) | italian industries data | DNF |
| [limpidsoft](bots/limpidsoft/titles.ipynb) | pdf of old books | ~100 |
| [London Marathon](bots/london_marathon/fetch_details_urls.py) | download marathon results (since 2014)  | ~175000 |
| [NYC Marathon](bots/nyc_marathon/fetch_details.py) | download full marathon results (since 1970)  | ~1 million |
| [http://prdrklaina.weebly.com/](bots/prdrklaina/prdrklaina.ipynb) | download books | 16 |
| [Rotten Tomaroes](bots/rottentomatoes/cli.py) | browse [Rotten Tomatoes](https://www.rottentomatoes.com/) database | DNF |
| [REWE](bots/rewe/rewe_favorites.ipynb) | save your favorite [REWE](https://shop.rewe.de/) articles | DNF |
| [Stats F1](bots/statsf1/cli.py) | scrapes [Stats F1](http://statsf1.com/en/) | ~100000 |
| [The Creative Independent](bots/thecreativeindependent) | Shows highlighted text in thecreativeindependent.com articles | DNF |
| [The Guardian](bots/the-guardian) | The Guardian TOP 50 | 50 |
| [Tour De France](bots/letour/cli.py) | results of Tour de France since 1903 | ~160000 |
| [ultra-marathon statisik](bots/statistik_ultramarathon) | ultra-marathon performances | ~3 million |


## TODO bots
| name | description | data size |
| ------------- | ------------- | ------------- |
| [Giro d'Italia](http://www.giroditalia.it/it/classifiche/) | results of Giro d'Italia | ~3500 |
| [Giochi PRISTEM Bocconi](http://matematica.unibocconi.it/articoli/archivio-giochi) | archive of national italian games | ~194 |
| [Progetto Olimpiadi della Matematica](http://olimpiadi.dm.unibo.it/area-downloads/) | archive of major italian maths games | ~120 |


## Install
Just run `pip3 install . --upgrade --force-reinstall`: this will take care of all dependencies.
If you run into errors, you may have to install [pyhal](https://github.com/sirfoga/pyhal).


## Contributing
[Fork](https://github.com/sirfoga/scrapebots/fork) | Patch | Push | [Pull request](https://github.com/sirfoga/scrapebots/pulls)


## Feedback
Suggestions and improvements [welcome](https://github.com/sirfoga/scrapebots/issues)!


## Authors
| [![sirfoga](https://avatars0.githubusercontent.com/u/14162628?s=128&v=4)](https://github.com/sirfoga "Follow @sirfoga on Github") |
|---|
| [Stefano Fogarollo](https://sirfoga.github.io) |


## License
[MIT License](https://opensource.org/licenses/MIT)
